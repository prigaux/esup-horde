// Just to have syntax hilighting in vim ...
// <?php	
/*
 * "Stack" this method on top of the real _connect() method for class Ingo_Transport_Timsieved
 * This method replace the original method through the "runkit" PHP hack
 * The "real" _connect method is renamed __connect
 * Loaded by the CAS auth handler
 */

// protected function _connect()
// {
    $log_head = get_class($this) . '::_connect (rewritten) ';
	Horde::logMessage($log_head . 'Starting', 'DEBUG');

    $n_tryLogin = 1;
    while ($n_tryLogin <= 3) {	# Boucle CAS, pour renouveler le Proxy Ticket si besoin
        try {
            $this->__connect();
		    return;  # On sort de la boucle CAS
        } catch (Ingo_Exception $e) {
            Horde::logMessage($log_head . 'Catching exception after __connect, n_tryLogin=' . $n_tryLogin, 'DEBUG');
            include_once('CAS.php');
            //TODO if (! phpCAS::isAuthenticated() || $n_tryLogin > 2) {
            if ($n_tryLogin > 3) {
                throw $e; // The exception goes upward
                break;
            }
		    $n_tryLogin++;
            // The login has failed, we try to renew the password (CAS Proxy Ticket)
            Horde::logMessage($log_head . 'Trying to renew the password', 'INFO');
            $auth = $GLOBALS['injector']->getInstance('Horde_Core_Factory_Auth')->create();   
            // Renvoie un object de classe "Horde_Core_Auth_Application" 
            // http://dev.horde.org/api/framework/Core/Core/Horde_Core_Auth_Application.html
            // On essaie une nouvelle authentification transparente. Le module CAS recharge un nouveau Proxy Ticket !
            $auth->transparent(); // TODO verify return value ...
            $credentials = $auth->getCredential('credentials');
            $this->_params['password'] = $credentials['password'];
        	// Encrypt password (Stolen from  Horde/Imap/Client/Base.php)
            Horde::logMessage($log_head . 'New password ' . $this->_params['password'], 'INFO');
            if ($this->_params['_passencrypt']) {
                Horde::logMessage($log_head . 'Trying to encrypt the password', 'DEBUG');
                $encrypt_key = $this->_getEncryptKey();
                $secret = new Horde_Secret();
                $this->_params['password'] = $secret->write($encrypt_key, $this->_params['password']);
            }
        }
    }	# Fin de la boucle CAS
    Horde::logMessage($log_head . 'Ending', 'DEBUG');
// }
