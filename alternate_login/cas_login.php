<?php
/**
 * Copyright 1999-2015 Horde LLC (http://www.horde.org/)
 *
 * See the enclosed file COPYING for license information (LGPL-2). If you
 * did not receive this file, see http://www.horde.org/licenses/lgpl.
 *
 * @author   Chuck Hagenbuch <chuck@horde.org>
 * @category Horde
 * @license  http://www.horde.org/licenses/lgpl LGPL-2
 * @package  Horde
 */

// Edit the following line to match the filesystem location of your Horde
// installation.

// CAS Login


require_once('CAS.php');

$HORDE_DIR = __DIR__;
require_once __DIR__ . '/lib/Application.php';

/* Initialize Horde environment. */
Horde_Registry::appInit('horde', array(
    'authentication' => 'none',
));



phpCAS::proxy(CAS_VERSION_2_0, $conf['cas']['host'], $conf['cas']['port'], $conf['cas']['context']);


if ( isset($conf['cas']['PGTStorageDb']['db']) ) {
    $db          = $conf['cas']['PGTStorageDb']['db'];
    $db_user     = $conf['cas']['PGTStorageDb']['user'];
    $db_password = $conf['cas']['PGTStorageDb']['pass'];
    $db_table    = $conf['cas']['PGTStorageDb']['table'];
    phpCAS::setPGTStorageDb($db, $db_user, $db_password, $db_table);
} else {
    phpCAS::setPGTStorageFile ($conf['cas']['PGTStorageDir']);
}


if (! empty($conf['cas']['cas_cacert'])) {
        phpCAS::setCasServerCACert ($conf['cas']['cacert']);
} else {
        phpCAS::setNoCasServerValidation();
}


function onSingleSignout($ticket) {
    $ticket = preg_replace('/[^a-zA-Z0-9\-]/', '', $ticket);
    
    $sessionid = $GLOBALS['injector']->getInstance('Horde_Cache')->get("cas_login.php:$ticket", 0);
    if ($sessionid) {
        session_id($sessionid);
        session_start();
        session_unset();
        session_destroy();
    }
    exit;
}
phpCAS::setSingleSignoutCallback('onSingleSignout');
phpCAS::handleLogoutRequests();

phpCAS::forceAuthentication();

$ticket = session_id();
$username = phpCAS::getUser();
$password = phpCAS::retrievePT($conf['imap_service_name'] , $err_code, $output);

// Horde Login
$auth = $injector->getInstance('Horde_Core_Factory_Auth')->create();


// Check for CAS authentication.
if (empty($username) ||
    empty($password) ||
    !$auth->authenticate($username, array('password' => $password))) {
    $auth->getError();
}

$one_day = 86400;
$injector->getInstance('Horde_Cache')->set("cas_login.php:$ticket", session_id(), 2 * $one_day);

require HORDE_BASE . '/index.php';
exit;

