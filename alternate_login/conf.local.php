$conf['session']['max_time'] = 72000;

$conf['auth']['alternate_login'] = 'https://'.$_SERVER['SERVER_NAME'].'/cas_login.php';
$conf['auth']['params']['app'] = 'imp';
$conf['auth']['driver'] = 'application';

$conf['cas']['host']    = 'cas.xxx.fr';
$conf['cas']['port']    = 443;
$conf['cas']['context'] = '/cas';
$conf['cas']['cacert']  = '';


// On choisi la méthode pour le PGT sachant que pour un service load balancé le PGTStorageDb est obligatoire
// Pour la méthode PGTStorageDb il faut créer une table dans la base de données :
//
// CREATE TABLE IF NOT EXISTS `cas` (
//  `pgt_iou` varchar(255) NOT NULL,
//  `pgt` varchar(255) NOT NULL,
//  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
//  PRIMARY KEY (`pgt_iou`)
// ) ENGINE=MyISAM DEFAULT CHARSET=utf8;

// PGT StorageDir
// $conf['cas']['PGTStorageDir']  = '/tmp';
// PGT Storage 
$conf['cas']['PGTStorageDb']['db']    = 'mysql:host=' . $conf['sql']['hostspec'] .';dbname=' . $conf['sql']['database'];
$conf['cas']['PGTStorageDb']['user']  = $conf['sql']['username'];
$conf['cas']['PGTStorageDb']['pass']  = $conf['sql']['password'];
$conf['cas']['PGTStorageDb']['table'] = 'cas';

$conf['imap_service_name'] = 'imap://imap.xxx.fr';

$conf['auth']['redirect_on_logout']               = 'https://' . $conf['cas']['host'] . $conf['cas']['context'] . '/logout?service=https://'.$_SERVER['SERVER_NAME'];

