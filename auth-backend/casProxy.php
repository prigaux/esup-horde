<?php
/**
 * $Horde: horde/casProxy.php,v 2.0 2011/07/29 $
 *
 * Copyright Consortium Esup-Portail (http://www.esup-portail.org)
 *
 * See the enclosed file COPYING for license information (LGPL). If you
 * did not receive this file, see http://www.fsf.org/copyleft/lgpl.html.
 *
 * @author  Julien Marchal <julien.marchal@univ-nancy2.fr>
 * @author  Xavier Montagutelli <xavier.montagutelli@unilim.fr>
 */

require_once './lib/Application.php';
require_once HORDE_BASE .'/config/conf.php';
if (!empty($GLOBALS['conf']['vhosts'])) {
	include_once HORDE_BASE . '/config/conf-' . $GLOBALS['conf']['server']['name'] . '.php';
}

if(! isset($_GET['pgtIou'])){
    @session_destroy();
    exit(0);
}

try {
    Horde_Registry::appInit('horde', array('authentication' => 'none', 'nologintasks' => true));
} catch (Horde_Exception $e) {}

$vars = Horde_Variables::getDefaultVariables();
$is_auth = $registry->isAuthenticated();

/* This ensures index.php doesn't pick up the 'url' parameter. */
$horde_login_url = '';

/* Initialize the Auth credentials key. */
if (!$is_auth) {
    $injector->getInstance('Horde_Secret')->setKey('auth');
}

/* Get an Auth object. */
$auth = $injector->getInstance('Horde_Core_Factory_Auth')->create(($is_auth && $vars->app) ? $vars->app : null);


// $auth = &Auth::singleton($conf['auth']['driver']);
	
if (is_a($auth, 'PEAR_Error')) {
	Horde::fatal($auth, __FILE__, __LINE__);
}
// must load driver if use composite auth
if(is_a($auth,"Auth_composite")) {
	$auth->authenticate("none","none");
}
	
@session_destroy();
?>
