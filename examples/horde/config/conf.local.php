<?php
$conf['testdisable'] = true;

// redirect back to make sure there's no frame-in-frame when sth goes wrong
// Test XM - From http://wiki.horde.org/CASAuthHowTo
$conf['auth']['alternate_login'] = 'https://cas.univ.fr/cas/login?service=https://'.$_SERVER['SERVER_NAME'];
//
// Partie manuelle pour la definition du driver d'authentification
// Pas d'utilisation du driver "Composite" (http://wiki.horde.org/AuthCompositeHowToH3)
//
// On definit deux driver, un pour l'acces web "normal", base sur CAS
// Le deuxieme est base sur LDAP et sera utilise pour l'acces RPC
// La selection se fait via l'URI directement ici, tout simplement
//
if ( Horde_Cli::runningFromCLI() || 
     preg_match('|^/rpc.php|',$_SERVER['REQUEST_URI']) ||
     preg_match('|^/Microsoft-Server-ActiveSync|',$_SERVER['REQUEST_URI']) ||
     preg_match('|^/autodiscover/autodiscover.xml|i',$_SERVER['REQUEST_URI']) ||
     preg_match('|^/kronolith/fb.php|',$_SERVER['REQUEST_URI']) ||
     preg_match('|^/services/imple.php|',$_SERVER['REQUEST_URI'])
   ) {
    // Driver LDAP
    // Les autres parametres (nom du serveur, binddn) sont ceux  de la conf LDAP generale
    $conf['auth']['driver'] = 'ldap';
    $conf['auth']['params']['basedn'] = 'dc=univ,dc=fr';
    $conf['auth']['params']['scope'] = 'sub';
    $conf['auth']['params']['ad'] = false;
    $conf['auth']['params']['uid'] = 'uid';
    $conf['auth']['params']['encryption'] = 'ssha';
    $conf['auth']['params']['newuser_objectclass'] = array();
    $conf['auth']['params']['filter'] = '(objectclass=supannPerson)';
    $conf['auth']['params']['password_expiration'] = 'no';
    $conf['auth']['params']['driverconfig'] = 'horde';
} else {
    // Driver CAS
    $conf['auth']['driver'] = 'cas';

    $conf['auth']['params']['cas_host'] = 'cas.univ.fr';
    $conf['auth']['params']['cas_port'] = 443;
    $conf['auth']['params']['cas_context'] = '/cas';
    $conf['auth']['params']['cas_cacert'] = '/etc/ssl/certs/AddTrust_External_Root.pem';
    $conf['auth']['params']['cas_callback_url'] = 'https://webmail.univ.fr/casProxy.php';
    $conf['auth']['params']['cas_renew_password'] = false;  // To renew the password *with the runkit extension*
    $conf['auth']['params']['cas_proxy'] = true;
    $conf['auth']['params']['cas_handle_logout'] = true;
    $conf['auth']['params']['basedn'] = 'dc=univ,dc=fr';
    $conf['auth']['params']['scope'] = 'sub';
    $conf['auth']['params']['driverconfig'] = 'horde';
    $conf['auth']['params']['ldap'] = $conf['ldap'];
    $conf['auth']['params']['debuglevel'] = 'INFO';
    $conf['auth']['params']['cas_debug'] = '/tmp/phpCAS.log';
	$conf['auth']['params']['uid'] = 'uid';
	$conf['auth']['params']['filter'] = '(&(objectclass=supannperson)(!(employeeType=not_member)))';

//    $conf['auth']['params']['cas_pgtdir'] = '/tmp';

    // Also do a CAS logout after disconnecting from Horde.
    $conf['auth']['redirect_on_logout'] = 'https://' . $conf['auth']['params']['cas_host'] . $conf['auth']['params']['cas_context'] . '/logout';


}

?>
